//Example a = 7;
// b = 3
// a = a + b = 10
// b = a - b = 4
// a = (a-b) / 2
// b = b + a   

// Write a number to swap integers without using extra variables

#include <iostream>

using namespace std;

void swap(int& a, int& b){
	a = a + b; //10
	b = a - b; //7
	a = (a - b);
}

int main(){
	int a = 700;
	int b = 33;
	swap(a, b);

	cout << "a: " << a;
	cout << " b: " << b;
}