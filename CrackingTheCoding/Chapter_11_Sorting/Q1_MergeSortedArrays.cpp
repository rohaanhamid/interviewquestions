// Merge sorted arrays a and b where a has enough space to hold b.

// a = {5, 8, 10, 0, 0, 0}
// b = {1 ,2 ,9}

// a = {5, 8, 5, 8, 9, 10}
// b = {1 ,2 ,9}

// a = {5, 8, 10, 0, 0, 0}
// b = {1 ,2 ,9}

void merge(int* a, int* b, int lenA, int lenB){
	int indexA = lenA - lenB - 2;
    int indexB = lenB - 1;
    int indexAB = lenA - 1;

	while(lastA && lastB){
		if(a[indexA] > b[indexB]){
			a[indexAB] = a[indexA];
			indexA--;
		}else if(a[indexA] < b[indexB]){
			a[indexAB] = b[indexB];
			indexB--;
		}
		indexAB--;
	}

	while(indexB > 0){
		a[indexAB] = b[indexB];
		indexB--;
		indexAB--;
	}
}