//Given a BST create a lists using nodes at each depth.
#include "utils/binary_tree_pretty_print.cpp"
#include <list>
#include <vector>
#include <map>

typedef map<int,list<int> > ListMap;

//Can be an in order traversal
void addToList(ListMap& lists, BinaryTree* root, int levelNumber){

	(lists[levelNumber]).push_back(root->data);

	if(root->left){
		addToList(lists, root->left, levelNumber + 1);
	}
	if(root->right){
		addToList(lists, root->right, levelNumber + 1);
	}
}

ListMap convertToLists(BinaryTree* root){
	ListMap lists;
	addToList(lists, root, 0);
	return lists;
}

int main(){
	BinaryTree* bst = new BinaryTree(4);
	bst->left = new BinaryTree(2);
	bst->right = new BinaryTree(6);
	bst->left->left = new BinaryTree(3);
	bst->left->right = new BinaryTree(5);


	ListMap lists = convertToLists(bst);

	for(int i = 0; i < lists.size(); i++){
		for(list<int>::iterator j = (lists[i]).begin(); j != (lists[i]).end(); ++j){
			cout << *j << " ";
		}
		cout << endl;
	}
}

