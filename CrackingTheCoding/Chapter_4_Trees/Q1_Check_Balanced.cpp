#include "utils/binary_tree_pretty_print.cpp"
#include <algorithm>

int findMinHeight(BinaryTree* root){
	if(root == NULL){
		return 0;
	}
	return 1 + min(findMinHeight(root->left), findMinHeight(root->right));
}

int findMaxHeight(BinaryTree* root){
	if(root == NULL){
		return 0;
	}
	return 1 + max(findMaxHeight(root->left), findMaxHeight(root->right));
}

bool checkBalanced(BinaryTree* root ){
	if(root == NULL){
		return false;
	}

	if(findMinHeight(root) == findMaxHeight(root)){
		return true;
	}

	return false;
}

int main(){
	  BinaryTree *root = new BinaryTree(30);
	  root->left = new BinaryTree(20);
	  root->right = new BinaryTree(40);
	  root->left->left = new BinaryTree(10);
	  root->left->right = new BinaryTree(25);
	  root->right->left = new BinaryTree(35);
	  root->right->right = new BinaryTree(50);
	  root->left->left->left = new BinaryTree(5);
	  root->left->left->right = new BinaryTree(15);
	  root->left->right->right = new BinaryTree(28);
	  root->right->right->left = new BinaryTree(41);

	  cout << "Tree pretty print with level=1 and indentSpace=0\n\n";
	  // Output to console
	  printPretty(root, 1, 0, cout);

	  cout << "The tree is: ";
	  (checkBalanced(root)) ? cout << ("balanced") : cout << ("unbalanced");

	  BinaryTree *root1 = new BinaryTree(30);
	  root1->left = new BinaryTree(20);
	  root1->right = new BinaryTree(40);

	  cout << "Tree pretty print with level=1 and indentSpace=0\n\n";
	  // Output to console
	  printPretty(root1, 1, 0, cout);

	  cout << "The tree is: ";
	  (checkBalanced(root1)) ? cout << ("balanced") : cout << ("unbalanced");
}

