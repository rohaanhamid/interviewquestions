#include "utils/binary_tree_pretty_print.cpp"

bool findNode(BinaryTree* root, BinaryTree* node){
	if(!root){
		return false;
	}

	if(root == node){
		return true;
	}

	return findNode(root->left, node) || findNode(root->right, node);
}

BinaryTree* findLeastCommmonAncestor(BinaryTree* root, BinaryTree* A, BinaryTree* B){
	if(!root){
		return NULL;
	}
	bool A_in_left = findNode(root->left, A);
	bool B_in_left = findNode(root->left, B);
	bool A_in_right = findNode(root->right, A);
	bool B_in_right = findNode(root->right, B);

	if(A_in_left && B_in_left){
		return findLeastCommmonAncestor(root->left, A, B);
	}

	if(A_in_right && B_in_right){
		return findLeastCommmonAncestor(root->right, A, B);
	}

	return root;

}

int main(){
	BinaryTree* bst = new BinaryTree(4);
	bst->left = new BinaryTree(2);
	BinaryTree* A = bst->right = new BinaryTree(6);
    bst->left->left = new BinaryTree(3);
	bst->left->right = new BinaryTree(5);
	BinaryTree* B = bst->left->right->right = new BinaryTree(10);

	printPretty(bst, 1, 0, cout);
	BinaryTree* res = findLeastCommmonAncestor(bst, A, B);

	cout << "Common Ancestor" << res->data;
}
