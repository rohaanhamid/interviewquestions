#include "utils/binary_tree_pretty_print.cpp"

//Create a binary tree from Sorted array

BinaryTree* createBinaryTree(int array[], int start_index, int end_index){
	if(end_index < start_index){
		return NULL;
	}
	int mid = (end_index + start_index)/2;
	BinaryTree* root = new BinaryTree(array[mid]);
	root->left = createBinaryTree(array, start_index, mid-1);
	root->right = createBinaryTree(array, mid+1, end_index);
	return root;
}

BinaryTree* createBinaryTree(int array[], int length){
	return createBinaryTree(array, 0, length-1);
}

int main(){
	int sorted_array[7] = {1,2,3,4,5,6};
	printPretty(createBinaryTree(sorted_array, 7), 1, 0, cout);
}


