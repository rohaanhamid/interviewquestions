//Question 1.1:Implement an algorthm to determine if string has all characters

bool hasAllUniqueCharacters(char* string){
    int asciiCharCounter[256]={0};
    
    for(int i=0; i<strlen(string); i++){
        if(asciiCharCounter[string[i]]++ > 1){
        	printf("Num chars %d\n",asciiCharCounter[string[i]]);
            return false;
        }
    }
    return true;
}

//Question 1.2: Write code to reverse C style string. String cstyle strings also have a null character. Remember to declare string as char str[]="something"
//Do this in place
void reverse(char* str){
    char* last=str;
    char temp;
    while(*last){
        ++last;
    }
    --last;
    
    while(str < last){
        temp=*last;
        *last=*str;
        *str=temp;
        --last;
        ++str;
    }
}

//Question 1.3: Write code to remove duplicate characters. This is buggy. Try it with two for loops, one identifies duplicates and the other removes them. Or try a more c-style approach
void removeDuplicates(string &str){
	bool ascii_chars[256]={false};
	for(int i=0; i<str.length(); i++){
		int val = str[i];
		if(ascii_chars[val]){
			str.erase(str.begin()+i);
			i--;
		}
		ascii_chars[val]=true;	
	}
}

//Question 1.4: Write a method to describe if two strings are anagrams or not
// int main(){
// 	char str1[] = "abcabc";
// 	char str2[] = "cbacbaa";
// 	areAnagrams(str1,str2) ? printf("True") : printf("False");
// }
bool areAnagrams(char* str1, char* str2){
    int asci_counter1[256]={0};
    int asci_counter2[256]={0};
    
    if(strlen(str1) != strlen(str2)){
        return false; 
    }
    
    for(unsigned int i=0; i < strlen(str1); i++){
        asci_counter1[str1[i]]++;
        asci_counter2[str2[i]]++;
    }
    
    for(unsigned int j=0; j < 256; j++){
        if(asci_counter1[j] != asci_counter2[j]){
            return false;
        }
    }
    return true;
}

//Question 1.5: Write a method to replace all the spaces in a string with '%20'
bool formatURL(string& str){
    int num_spaces = 0;
    int input_length = str.length();
    
    for(unsigned int i=0; i < input_length; i++){
        if(str[i] == ' '){
            num_spaces++;
	}
    }
    
    int new_length = input_length + (num_spaces*2);
    str.resize(new_length);
    //Read the string backwards and start filling from the end this time.
    for(int i=(input_length-1); i >=0; i--){
        if(str[i]==' '){
            str[new_length-1]='0';
            str[new_length-2]='2';
            str[new_length-3]='%';
            new_length= new_length-3;
        }
        else{
            str[new_length-1] = str[i];
            new_length = new_length -1;
        }
    }
}

//Question 1.6: Rotate by 90. Do a matrix transpose and then swap all the rows or columns. This is hard in C++
using namespace std;
void transpose(int** mat, int sizeOfEdge){
    for(int i=0; i < sizeOfEdge/2; i++){ //Remember that this is divided by 2.
        for(int j=0; j < sizeOfEdge/2; j++){
            int temp = mat[i][j]; 
            mat[i][j] = mat[sizeOfEdge-i-1][sizeOfEdge-j-1]; //Size of edge must be offset by 1 to match indexing
            mat[sizeOfEdge-i-1][sizeOfEdge-j-1] = temp;
        }   
    }
}

void swapColumns(int** mat, int sizeOfEdge){
    for(int i=0; i<sizeOfEdge; i++){
        for(int j=0; j < sizeOfEdge/2; j++){
            int temp=mat[i][j];
            mat[i][j]=mat[i][sizeOfEdge-j-1];
            mat[i][sizeOfEdge-j-1]=temp;
        }
    }
}

void printer(int** mat, int size){
	for(int i=0; i<size; i++){
		for(int j=0; j<size; j++)
			printf("%d ",mat[i][j]);
		printf("\n");
	}	
}

void rotateBy90(int** mat, int sizeOfEdge){
   // transpose[mat, sizeOfEdge];
   // swapColumns[mat, sizeOfEdge];
}

//Important to remember that you cannot pass arrays defined as int[row][col] in c++. They have to be created as pointers dynamically. Learn this syntax
int** create_matrix(int row, int col) 
{
    int **m = new int* [row];
    for(int i = 0; i < row; i++){
        m[i] = new int[col];
    }

    return m;
}

int main(){
	int** matrix=create_matrix(2,2);
	matrix[0][0]=1;
	matrix[0][1]=2;
	matrix[1][0]=3;
	matrix[1][1]=4;
	

	printer(matrix, 2);
	transpose(matrix,2);
	printer(matrix, 2);
	swapColumns(matrix,2);
	printer(matrix, 2);
}










































