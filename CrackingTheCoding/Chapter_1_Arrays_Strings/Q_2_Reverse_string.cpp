#include <string>
#include <iostream>

using namespace std;

void reverse(string& str){
	int end = str.size();
	for(int i = 0; i < end / 2; i++){
		char temp =  str[i];
		str[i] =  str[end-i-1];
		str[end-i-1] =  temp;
	}
}

int main(){
	string str = "abcdef";
	cout << "The string is: " << str;
	reverse(str);
	cout << "The string reversed is: " << str;
}
