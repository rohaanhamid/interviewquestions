#include <string>
#include <stdio.h>
#include <iostream>

using namespace std;

bool hasAllUniqueCharacters(string str){
    int asciiCharCounter[256]={0};
    
    for(int i=0; i< str.size(); i++){
        if(++asciiCharCounter[str[i]] > 1){
            return false;
        }
    }
    return true;
}

int main(){

	string str1 = "abcdefgh";
	string str2 = "test";
	cout << "\nThe string is: " << str1;
	cout << "\nCheck all unique characters: ";
	hasAllUniqueCharacters(str1) ? cout << ("true\n") : cout << ("false\n");
	cout << "\nThe string is: " << str2;
	cout << "\nCheck all unique characters: ";
	hasAllUniqueCharacters(str2) ? cout << ("true\n") : cout << ("false\n");

}
