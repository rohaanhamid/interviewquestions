#include <string>
#include <iostream>

using namespace std;

void removeDuplicates(string &str){
	bool ascii_chars[256]={false};
	for(int i=0; i<str.length(); i++){
		int val = str[i];
		if(ascii_chars[val]){
			str.erase(i, 1);
			i--;
		}
		ascii_chars[val]=true;
	}
}

int main(){
	string str = "tttt";
	cout << "String is:" << str;
	removeDuplicates(str);
	cout << "String with removed characters: " << str;
}
