int nth_Fibonacci(int n){
	if(n == 0){
		return 0;
	}else if(n == 1){
		return 1;
	}

	return nth_Fibonacci(n-1) + nth_Fibonacci(n-2);
}
