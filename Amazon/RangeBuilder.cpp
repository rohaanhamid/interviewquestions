
// Given an array of ints, return a string identifying the range of numbers

// Example
// Input arr - [0 1 2 7 21 22 1098 1099]
// Output - "0-2,7,21-22,1098-1099"

#include <string>
#include <iostream>
#include <sstream>

using namespace std;

string returnRangeString(int array[], int length){
	stringstream ss;
	int begin = 0;
	for(int i = 1; i <= length; i++){
		cout << array[i] << "\n";
		if((array[i] - array[i-1]) > 1 || i==length){
			if(begin == i-1){
				ss << array[begin] << ',';
			}else{
				ss << array[begin] << '-' << array[i-1] << ',';
			}
			begin = i;
		}
	}
	return ss.str();
}

int main(){
	int arr[9]= {0, 1, 2, 7, 21, 22, 1098, 1099, 2005};
	cout << returnRangeString(arr, 9);
	return 0;
}