// Merge two sorted linked lists into one
// ptr1 -> 3 -> 4 -> 9
// ptr2 -> 1 -> 2 -> 5
// ptr3 -> 1
#include <iostream>
#include <list>

using namespace std;

typedef list<int> intList;

struct node{
	int data;
	node* next;
};

node* mergeSortedLists(node* l1, node* l2){
	if(!l1 || !l2){
		return NULL;
	}

	//Try using a queue to store the pointers then read it off back into a list
	node* result = new node;

	node* ptr1 = l1;
	node* ptr2 = l2;
	node* ptr3 = result;

	while(1){
		if(!ptr1){
			ptr3->next =  ptr2;
			break;
		}else if(!ptr2){
			ptr3->next =  ptr1;
			break;
		}

		if(ptr1->data <= ptr2->data){
			ptr3->next = ptr1;
			ptr1 = ptr1->next;
		}else{
			ptr3->next = ptr2;
			ptr2 = ptr2->next;
		}
		ptr3 = ptr3->next;
	}

    node* temp = result;
	result = result->next;
	delete temp;
	return result;
}

int main(){
	int arr1[5] = {1,3,4,6,7};
	int arr2[5] = {2,5,6,8,9};

	node* list1;
	node* list2;

	node* ptr1;
	node* ptr2;

	list1 = ptr1;
	list2 = ptr2;

	for(int i = 0; i < 5; i++){
		ptr1 = new node;
		ptr2 = new node;
		ptr1->data = arr1[i];
		ptr2->data = arr2[i];
		ptr1 =  ptr1->next;
		ptr2 =  ptr2->next;
	}

	node* merged = mergeSortedLists(list1, list2); 
	// while(merged){
	// 	cout << merged->data << endl;
	// 	merged =  merged->next;
	// }

}

