// Return the longest subset of numbers with a difference of 1

// Given a list of integers, e.g.:

// { 2, 6, 7, 9, 1, 0, 1, 2, 3, 6 }

// .
// Write an time efficient algorithm to find the longest subset in which the difference between the minimum and maximum numbers is 0 or 1.
// The subset can have a length of 0 and can hold any of the values in the original array (order not matters).

#include <iostream>
#include <stdlib.h>

using namespace std;

struct limits{
	int beginning;
	int end;
};

limits findLongestSubset(int array[], int length){
	limits lim;
	lim.beginning = 0;
	lim.end = 0;

	int tail = 0;
	for(int i = 1; i <= length; i++){
		if(abs(array[i] - array[i-1]) > 1 || i==length){
			int diff = i-1-tail;
			if(diff > (lim.end - lim.beginning)){
				lim.beginning = tail;
				lim.end = i-1;
			}
			tail = i;
		}
	}

	return lim;
}

int main(){
	int array[10] = { 2, 6, 7, 9, 10, 0, 1, 2, 3, 6 };
	//i//nt array[5] = { 2, 6, 7 , 9, 11};

	limits lim =  findLongestSubset(array, 10);
	cout << "beg: " << lim.beginning << "end: " << lim.end;

}