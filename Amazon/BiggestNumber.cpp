// Find the biggest number from given numbers
// Make a mini hash table with number as key. This can just be an array.

#include <iostream>

using namespace std;

//NOTE: FOR SIMPLICITY NUMBER CAN BE CONVERTED TO STRING AND ACCESSED BUT str.charAt();

int getBiggestNumber(int number){
	int counter[10] = {0};
	int i=1;
	while(1){
		int temp = number/(i);
		if(!temp){
			break;
		}
		int digit = (temp %  (10));
		counter[digit]++;
		i = i*10;
	}

	i=1;
	int result = 0;
	for(int j=0; j < 10; j++){
		for(int k=0; k < counter[j]; k++){
			result += i*j;
			i = i*10;
		}
	}
	return result;
}

int main(){
	cout << getBiggestNumber(2693090);
}