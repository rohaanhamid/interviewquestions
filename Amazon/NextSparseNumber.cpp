#include <iostream>

// Sparse number is an integer if there are no adjacent 1 in it's binary representation.
// Like: 5 -> 101 (no adjacent 1)
// 9 -> 1001 (no adjacent 1)
// while 6-> 110 is not sparse number.
// Now you are given an integer find the NEXT BIGGER sparse number.Please mind 'it is next bigger'.

using namespace std;

int findNextBigSparse(int number){

	int length = (sizeof(int) * 8);

	int mask = 1;
	int indexOfLastOne = -1;

	for(int i = 1; i < length; i++){
		if(number & (mask<<i) && number & (mask<<(i-1))){
			return 0;
		}
	}

	for(int j = length - 1; j > 0; j--){
		if(number & (mask<<i)){
			indexOfLastOne = j;
			cout << "Index " << indexOfLastOne << endl;
		}
	}

	return number | (mask << (indexOfLastOne + 2));
}

int main(){
	cout << findNextBigSparse(5); 
	return 0;
}