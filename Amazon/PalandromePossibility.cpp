#include <iostream>
#include <cstring>

using namespace std;

bool checkPalandromability(char input[]){
	// Count number of occurences of all the characters. More than 1 num occurences should be an even number

	bool odd_ascii_occurrence[256] = {false};
	int length = strlen(input);

	if(length < 2){
		return false;
	}

	for(int i = 0 ; i < strlen(input); i++){
		odd_ascii_occurrence[input[i]] = !odd_ascii_occurrence[input[i]];
	}

	int num_odd_occurences = 0;
	for(int j = 0; j < 256; j++){
		if(odd_ascii_occurrence[j]){
			num_odd_occurences++;
		}
	}
	return num_odd_occurences <= 1;
}

int main(){
	char string[30] = "HHAN";
	cout << checkPalandromability(string) << "\n";
}