//Find the second most repeating number in an array
#include <iostream>
#include <map>
#include <iterator>

using namespace std;

typedef map<int, int> counter;

// Generic Implementation requiring quite a bunch of extra space
int findKthMostRepeatingNumber(int k, int array[], int length){
	
	if( k > length){
		return -1;
	}

	counter c1;
	counter c2;

	for(int i = 0; i < length; i++){
		c1[array[i]]++ ;
	}

	for(counter::iterator it = c1.begin(); it!=c1.end(); ++it){
		cout << it->first << " Count " << it->second << "\n";
		c2[it->second] = it->first;
	}
	
	counter::iterator it = c2.begin();
	advance(it, k-1);

	cout << "Number " << it->second;

	return 0;
}

// Assuming all numbers are positive
int findSecondMostRepeating(int array[], int length){
	int currMostRepeating = 0;
	int currSecondMostRepeating = 0;
	int currMax = 0;
	int currSecondMax = -1;
	for(int i = 0; i < length; i++){
		int currNumber = array[i];
		int currCounter = 0;
		if(currNumber > 0){
			for (int j = i; j < length; j++)
			{
				if(currNumber == array[j]){
					currCounter++;
					array[j] = -array[j];
				}

			}

			if(currCounter > currMax){
				currSecondMax = currMax;
				currSecondMostRepeating = currMostRepeating;
				currMax = currCounter;
				currMostRepeating = -array[i];
			}
			else if(currCounter > currSecondMax && currCounter < currMax){
				currSecondMax = currCounter;
				currSecondMostRepeating = -array[i];
			}
		}
	}
	return currSecondMostRepeating;
}

int main(){
	int array[10] = {10,1,3,4,5
					,5,5,6,7,1};

	//findKthMostRepeatingNumber(2,array, 10);
	cout << findSecondMostRepeating(array, 10);
	return 0;
}

// Assumptions extra storage allowed